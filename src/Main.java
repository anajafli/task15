import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println("1. Create a list of integers and find the maximum and minimum values.");

        ArrayList<Integer> mylist = new ArrayList<>();

        mylist.add(15);
        mylist.add(18);
        mylist.add(32);
        mylist.add(75);
        mylist.add(5);
        mylist.add(12);
        mylist.add(199);
        mylist.add(33);
        mylist.add(8);
        mylist.add(25);
        mylist.add(4);
        mylist.add(10);
//Part 1
        System.out.println();
        int max = mylist.get(0);
        int min = mylist.get(0);

        for (int i=1; i<mylist.size(); i++){
            if(mylist.get(i)>max) max = mylist.get(i);
            if(mylist.get(i)<min) min = mylist.get(i);

        }

        System.out.println("List of Integers: " + mylist);
        System.out.println("Maximum Value: " + max);
        System.out.println("Minimum Value: " + min);
//Part 2
        System.out.println("2. Create a list of integers and remove all elements from a list that are divisible by 3.");
        System.out.println("deleted elements");
        for (int i=0; i<mylist.size(); i++) {
                if (mylist.get(i)%3 == 0){
                    System.out.print( mylist.get(i)+" ");
                    mylist.remove(i);}
    }
//Part 3
        System.out.println();
        System.out.println("3. Check if a list of strings contains a specific element.");
ArrayList<String>mystring = new ArrayList<>();
        mystring.add("Afat");
        mystring.add("Najafli");
        if(mystring.contains("Afat")){
            System.out.println("List contain Afat");
        }
        else System.out.println("List not contain Afat");
//Part 4
        System.out.println("4. Find the average of all even numbers in a list of integers.");
        int sum = 0;
        int count = 0;
for(int i=0;i<mylist.size(); i++){
    if(mylist.get(i)%2==0){
        count+=1;
        sum+=mylist.get(i);

    }
}



        System.out.println("Average of even numbers : "+ sum/count);






        }


}