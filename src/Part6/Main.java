package Part6;

public class Main {
    public static void main(String[] args) {
        StudentRecordSystem recordSystem = new StudentRecordSystem();

        Student student1 = new Student(750, "Ali", 95);
        Student student2 = new Student(102, "Afat", 96);
        Student student3 = new Student(109, "Najaf", 71);

        recordSystem.addStudent(student1);
        recordSystem.addStudent(student2);
        recordSystem.addStudent(student3);

        System.out.println("Students:");
        recordSystem.printAllStudents();

        int studentId = 102;
        Student retrievedStudent = recordSystem.getStudentById(studentId);
        if (retrievedStudent != null) {
            System.out.println("\nStudent with ID " + studentId + ": " + retrievedStudent);
        } else {
            System.out.println("\nStudent with ID " + studentId + " not found.");
        }

        int studentIdToRemove = 109;
        recordSystem.removeStudent(studentIdToRemove);

        System.out.println("\nAfter removing student with ID " + studentIdToRemove + ":");
        recordSystem.printAllStudents();
    }
}