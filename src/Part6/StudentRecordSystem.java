package Part6;

import java.util.HashMap;

public class StudentRecordSystem {
    private HashMap<Integer, Student> studentRecords;

    public StudentRecordSystem() {
        studentRecords = new HashMap<>();
    }

    public void addStudent(Student student) {
        studentRecords.put(student.getId(), student);
    }

    public void removeStudent(int studentId) {
        studentRecords.remove(studentId);
    }

    public Student getStudentById(int studentId) {
        return studentRecords.get(studentId);
    }

    public void printAllStudents() {
        for (Student student : studentRecords.values()) {
            System.out.println(student);
        }
    }
}
