import java.util.HashMap;

public class Part5 {
    public static void main(String[] args) {
        HashMap<String, Integer>  myhashmap= new HashMap<String, Integer>();
        myhashmap.put("Desk" , 5);
        myhashmap.put("board", 11);
        myhashmap.put("door", 8);
        myhashmap.put("window", 16);

        myhashmap.replace("board", 11,19);
        myhashmap.remove("door");
        System.out.println( myhashmap.containsKey("window"));

        for(String i : myhashmap.keySet())
        {
            System.out.println(i+" "+myhashmap.get(i));
        }

    }


}
